defmodule PhoenixApi.HelloController do
  use PhoenixApi.Web, :controller

  def hello(conn, _params) do
    json conn, %{"hello": "world"}
  end
end
