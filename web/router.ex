defmodule PhoenixApi.Router do
  use PhoenixApi.Web, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", PhoenixApi do
    pipe_through :browser # Use the default browser stack

    get "/", PageController, :index
  end

  # Other scopes may use custom stacks.
  scope "/api", PhoenixApi do
    pipe_through :api

    get "/hello", HelloController, :hello
    get "/param/:name", ApiParamController, :rest_param1
    get "/param/:name/:age", ApiParamController, :rest_param2

    get "/param", ApiParamController, :get_param
    post "/param", ApiParamController, :post_param
    post "/json-param", ApiParamController, :json_param
    get "/file-param", ApiParamController, :file_param
    post "/file-param", ApiParamController, :upload_param

  end
end
