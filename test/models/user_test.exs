defmodule PhoenixApi.UserTest do
  use PhoenixApi.ModelCase

  alias PhoenixApi.User
  alias PhoenixApi.Repo

  @valid_attrs %{age: 42, email: "some content", name: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = User.changeset(%User{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = User.changeset(%User{}, @invalid_attrs)
    refute changeset.valid?
  end

  test "insert data" do
    changeset = User.changeset(%User{}, @valid_attrs)
    assert {:ok, _user} = Repo.insert(changeset)
  end

  test "delete data" do
    # insert for delete
    changeset = User.changeset(%User{}, @valid_attrs)
    {:ok, user} = Repo.insert(changeset)
    # delete
    assert {:ok, _user} = Repo.delete(user)
  end

  test "found data" do
    # insert for search
    changeset = User.changeset(%User{}, @valid_attrs)
    {:ok, user} = Repo.insert(changeset)
    # found data
    search_user = Repo.get(User, user.id)
    assert search_user.name === @valid_attrs[:name]
    assert search_user.email === @valid_attrs[:email]
    assert search_user.age === @valid_attrs[:age]
  end

  test "not found data" do
    # not found data
    not_found_user = Repo.get(User, -1)
    refute not_found_user
  end

  test "change data" do
    # insert for search
    changeset = User.changeset(%User{}, @valid_attrs)
    {:ok, user} = Repo.insert(changeset)

    # before change
    assert user.name === @valid_attrs[:name]
    assert user.email === @valid_attrs[:email]
    assert user.age === @valid_attrs[:age]
    # change
    changeset = User.changeset(user, %{name: "change name"})
    assert {:ok, new_user} = Repo.update(changeset)
    # after change
    assert new_user.name === "change name"
  end
end
